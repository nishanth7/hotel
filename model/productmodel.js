

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var Productschema   = new Schema({
    name: String,
    price:Number
});

module.exports = mongoose.model('Product', Productschema);