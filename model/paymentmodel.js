

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require("mongodb-autoincrement");
autoIncrement.setDefaults({
    collection: "counter_payment",     
    field: "_id",               
    step: 1             
});

var Paymentschema   = new Schema({
    _id:String,
	payment_mode:String,
		total_amount:String,
		
		roombooking_id:String
});
	Paymentschema.plugin(autoIncrement.mongoosePlugin);

module.exports = mongoose.model('room_payments', Paymentschema);