

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require("mongodb-autoincrement");
autoIncrement.setDefaults({
    collection: "counter_trip",
    field: "_id",
    step: 1
});

var Tripschema   = new Schema(
	{
	id:'string',
		date:'string',
		busId:'string',
		jeepId:'string',
		totalseats:'string',
	filledseats:'string',
	availableseats:'string',
	status:'string',
		userid:'string'
});
	Tripschema.plugin(autoIncrement.mongoosePlugin);

module.exports = mongoose.model('trip_booking', Tripschema);