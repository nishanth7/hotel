

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require("mongodb-autoincrement");
autoIncrement.setDefaults({
    collection: "counter_room",     
    field: "_id",               
    step: 1             
});

var Roomschema   = new Schema({
    _id:String,
	room_code: String,
    type:String,
});
	Roomschema.plugin(autoIncrement.mongoosePlugin);

module.exports = mongoose.model('room_master', Roomschema);