

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var autoIncrement = require("mongodb-autoincrement");
autoIncrement.setDefaults({
    collection: "counter_type",     
    field: "_id",               
    step: 1             
});

var Typeschema   = new Schema({
    _id:String,
	type:String,
	week_days_tarrif:Number,
	week_ends_tarrif:Number	
		
});
	Typeschema.plugin(autoIncrement.mongoosePlugin);

module.exports = mongoose.model('room_type', Typeschema);