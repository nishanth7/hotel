var express = require('express');
var router = express.Router();
var mongoose=require('mongoose');

/*
var Schema       = mongoose.Schema;

var Productschema   = new Schema({
    name: String,
    price:Number
});

var proM = mongoose.model('Product', Productschema);
*/
var productModel=require('../model/productmodel');


mongoose.connect('mongodb://localhost:27017/productNew'); 
//var Model=mongoose.model('Productschema',productModel);




router.get('/',function  (req,res) {
	res.send("welcome to product api");
});

router.get('/products',function  (req,res) 
{
    
    productModel.find(function(err,productlist)
                   {
                   		if(err)
                   		{
                   			res.send(err);
                   		}

                   		res.json(productlist);

                   }



    	);
	
});

router.post('/products',function(req,res)
{
	var pro = new productModel();

    pro.name=req.body.name;
    pro.price=req.body.price;

    pro.save(function(err)
    {
    		if(err)
    		{
    			res.send("unable to save");
    		}
    		else
    		{
    			res.send("the details saved");
    		}

    }



    );


});

router.put('/:productid',function(req,res)
{
    var id=req.params.productid;

    productModel.findById(id,function(err,product)
    {
      if(err)
      {
        res.send("unable to update check id");
      }
      else
      {
        
        product.name=req.body.name;
        product.price=req.body.price;
        product.save(function(err)
        {
          if(err)
          {
            res.send("unable to upadte");
          }
          else
          {
            res.send("product details upadted");
          }

        }



          );

      }



    }

    );

}

  );


router.delete('/:productid',function(req,res)
{
productModel.remove({_id:req.params.productid},function(err,product)
{
  if(err)
  {
res.send("unable to delete");
  }
  else
  {
    res.send("product deleted");
  }


});

}

);	
router.get('/regproduct',function(req,res)
{
  res.redirect('/public/regproduct.html');

});
/*
router.get('/updateproduct',function(req,res)
{
  res.redirect('/public/updateproduct.html');

});
*/
module.exports = router;