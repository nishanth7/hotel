// public/core.js
var scotchTodo = angular.module('scotchTodo', []);

function mainController($scope, $http) {
    $scope.formData = {};
	$scope.room = {};



    // when landing on the page, get all types of room and show them
    $http.get('/types/types')
        .success(function(data) {
            $scope.types = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


		$scope.submitForm = function() {
        // Posting data to php file
		var data = $.param({
                code: $scope.code,
                tid: $scope.tid,
                
            });
				var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('/rooms/rooms', data, config)
            .success(function (data, status, headers, config) {
                $scope.ServerResponse = data;
            })
            .error(function (data, status, header, config) {
                $scope.ServerResponse = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });

        
        };



		$scope.getavailablerooms = function() {
			
        // Posting data to node js
		var data = $.param({
				stype:$scope.stype,
                cin: $scope.cin,
                cout: $scope.cout,
                
            });
				var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('/bookings/getavailabledrooms', data, config)
            .success(function (data, status, headers, config) {
                $scope.ServerResponse = data;
				$scope.avrooms=data;
				$scope.avroom=$scope.avrooms[0];
            })
            .error(function (data, status, header, config) {
                $scope.ServerResponse = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });

        
        };
		$scope.bookroom = function(cin,cout,ty,id,email) {
			
        // Posting data to node js
		var data = $.param({
				stype:ty,
                cin: $scope.cin,
                cout: $scope.cout,
				rid:id,
				email:$scope.email,
                
            });
				var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('/bookingservice/bookroom', data, config)
            .success(function (data, status, headers, config) {
                $scope.ServerResponse = data;
				$scope.invoice=data;
            })
            .error(function (data, status, header, config) {
                $scope.ServerResponse = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });

        
        };

		/*$scope.getavailablerooms = function() {
        // Posting data to node js
		var data = $.param({
				
                cin: $scope.cin,
                cout: $scope.cout,
                
            });
				var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('/bookings/getavailabledrooms', data, config)
            .success(function (data, status, headers, config) {
                $scope.ServerResponse = data;
				$scope.avrooms=data;
            })
            .error(function (data, status, header, config) {
                $scope.ServerResponse = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });

        
        };*/

$scope.allocatedroom = function() {
        // Posting data to node js
		var data = $.param({
				stype:$scope.ttype,
                cin: $scope.cin,
                cout: $scope.cout,
                
            });
				var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('/bookings/allocate', data, config)
            .success(function (data, status, headers, config) {
                $scope.ServerResponse = data;
				$scope.avrooms=data;
            })
            .error(function (data, status, header, config) {
                $scope.ServerResponse = "Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config;
            });

        
        };


    // when submitting the add form, send the text to the node API
    $scope.createTodo = function() {
        $http.post('/api/todos', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

$scope.setType = function(ty) {
        $scope.ttype=ty;
    };


    // delete a todo after checking it
    $scope.deleteTodo = function(id) {
        $http.delete('/api/todos/' + id)
            .success(function(data) {
                $scope.todos = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}